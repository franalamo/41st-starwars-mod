  #include "\a3\editor_f\Data\Scripts\dikCodes.h"
_actualArray = (currentNamespace getVariable "fr4_units_defend");
{
	if (((toLower(typeOf _x)) find "jedi") >= 0) then
	{
		_actualArray pushBackUnique _x;
	};
	} forEach allUnits;
	currentNamespace setVariable ["fr4_units_defend",_actualArray];
	{
		currentNamespace setVariable [format ["fr4_%1_isDefending", _x],false];
		} forEach _actualArray;

//Add on/off button
["41st Controls","fr4_switch_sword", "Encender/Apagar Espada Laser", {_this call fr4_switch_sword}, "", [DIK_R, [false, true, true]],true] call CBA_fnc_addKeybind;
class CfgMovesMaleSdr: CfgMovesBasic
{
	class States
	{
		class Acts_AidlPercMstpSloWWpstDnon_warmup_1_loop;
		class CutSceneAnimationBaseConPistola: Acts_AidlPercMstpSloWWpstDnon_warmup_1_loop
		{
		};
		class Acts_A_M01_briefing;
		class CutSceneAnimationBaseSinPistola: Acts_A_M01_briefing
		{
		};
		class rgr_obiwan_point: CutSceneAnimationBaseConPistola
		{
			file = "\fr4_starwars_animations\anim\cutscenes\rgr_obiwan_point.rtm";
			speed = 1;
			actions = "PistolStandActions";
			ConnectTo[] = {"rgr_obiwan_point_loop",0.01};
			InterpolateTo[] = {};
		};
		class rgr_obiwan_point_loop: CutSceneAnimationBaseConPistola
		{
			file = "\fr4_starwars_animations\anim\cutscenes\rgr_obiwan_point_loop.rtm";
			speed = 1e-10;
			actions = "PistolStandActions";
			ConnectTo[] = {"AmovPercMstpSrasWpstDnon",0.01};
			InterpolateTo[] = {};
		};
		class rgr_pensativo: CutSceneAnimationBaseSinPistola
		{
			file = "\fr4_starwars_animations\anim\cutscenes\rgr_pensativo.rtm";
			speed = 1e-10;
			actions = "NoActions";
			ConnectTo[] = {"AmovPercMstpSnonWnonDnon",0.01};
			InterpolateTo[] = {};
		};
		class rgr_brazoscruzados: CutSceneAnimationBaseSinPistola
		{
			file = "\fr4_starwars_animations\anim\cutscenes\rgr_brazoscruzados.rtm";
			speed = 1e-10;
			actions = "NoActions";
			ConnectTo[] = {"AmovPercMstpSnonWnonDnon",0.01};
			InterpolateTo[] = {};
		};
		class rgr_cinturon: CutSceneAnimationBaseSinPistola
		{
			file = "\fr4_starwars_animations\anim\cutscenes\rgr_Cinturon.rtm";
			speed = 1e-10;
			actions = "NoActions";
			ConnectTo[] = {"AmovPercMstpSnonWnonDnon",0.01};
			InterpolateTo[] = {};
		};
		class rgr_espera: CutSceneAnimationBaseSinPistola
		{
			file = "\fr4_starwars_animations\anim\cutscenes\rgr_Espera.rtm";
			speed = 1e-10;
			actions = "NoActions";
			ConnectTo[] = {"AmovPercMstpSnonWnonDnon",0.01};
			InterpolateTo[] = {};
		};
		class rgr_facepalm: CutSceneAnimationBaseSinPistola
		{
			file = "\fr4_starwars_animations\anim\cutscenes\rgr_Facepalm.rtm";
			speed = 1e-10;
			actions = "NoActions";
			ConnectTo[] = {"AmovPercMstpSnonWnonDnon",0.01};
			InterpolateTo[] = {};
		};
		class rgr_victoria: CutSceneAnimationBaseSinPistola
		{
			file = "\fr4_starwars_animations\anim\cutscenes\rgr_Victoria.rtm";
			speed = 1e-10;
			actions = "NoActions";
			ConnectTo[] = {"AmovPercMstpSnonWnonDnon",0.01};
			InterpolateTo[] = {};
		};
		class rgr_meditar: CutSceneAnimationBaseSinPistola
		{
			file = "\fr4_starwars_animations\anim\cutscenes\rgr_meditar.rtm";
			speed = 1;
			actions = "NoActions";
			ConnectTo[] = {"AmovPercMstpSnonWnonDnon",0.01};
			InterpolateTo[] = {};
		};
		class rgr_meditar_loop: CutSceneAnimationBaseSinPistola
		{
			file = "\fr4_starwars_animations\anim\cutscenes\rgr_meditar_loop.rtm";
			speed = 1e-10;
			actions = "NoActions";
			ConnectTo[] = {"AmovPercMstpSnonWnonDnon",0.01};
			InterpolateTo[] = {};
		};
		class rgr_estrangulacion: CutSceneAnimationBaseSinPistola
		{
			file = "\fr4_starwars_animations\anim\cutscenes\rgr_Estrangulacion.rtm";
			speed = 1e-10;
			actions = "NoActions";
			ConnectTo[] = {"AmovPercMstpSnonWnonDnon",0.01};
			InterpolateTo[] = {};
		};
		class rgr_encendido: CutSceneAnimationBaseConPistola
		{
			file = "\fr4_starwars_animations\anim\cutscenes\rgr_EncendidoChulo.rtm";
			speed = 1e-10;
			actions = "NoActions";
			ConnectTo[] = {"AmovPercMstpSnonWnonDnon",0.01};
			InterpolateTo[] = {};
		};
	};
};
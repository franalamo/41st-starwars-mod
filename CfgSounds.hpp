class CfgSounds {
	sounds[] = {};
	class jetpack_start
	{
			// how the sound is referred to in the editor (e.g. trigger effects)
		name = "Jetpack Start";

			// filename, volume, pitch, distance (optional)
		sound[] = { "\fr4_starwars_animations\data\jetpackinicio.ogg", db+10, 1};

			// subtitle delay in seconds, subtitle text
		titles[] = {};
	};
	class jetpack_fly
	{
			// how the sound is referred to in the editor (e.g. trigger effects)
		name = "Jetpack fly";

			// filename, volume, pitch, distance (optional)
		sound[] = { "\fr4_starwars_animations\data\jetpackvolando.ogg", db+10, 1};

			// subtitle delay in seconds, subtitle text
		titles[] = {};
	};
};
class CfgMovesBasic
{
	class StandBase;
	class DefaultDie;
	class ManActions
	{
		fr4_defend1 = "fr4_defend1";
		fr4_defend2 = "fr4_defend2";
		fr4_defend3 = "fr4_defend3";
		rgr_guardia = "rgr_guardia";
		rgr_guardia_loop = "rgr_guardia_loop";
		rgr_guardia_halcon= "rgr_guardia_halcon";
		rgr_guardia_halcon_loop = "rgr_guardia_halcon_loop";
		rgr_GuardiaBaja = "rgr_GuardiaBaja";
		rgr_paseoJedi = "rgr_paseoJedi";
		fr4_fly_static = "fr4_fly_static";
		fr4_fly_to_flyforward = "fr4_fly_to_flyforward";
		fr4_flyforward = "fr4_flyforward";
	};
	class Actions
	{
		class Default;
		class NoActions: ManActions
		{
			fr4_defend1[] = {"fr4_defend1","Gesture"};
			fr4_defend2[] = {"fr4_defend2","Gesture"};
			fr4_defend3[] = {"fr4_defend3","Gesture"};
			rgr_guardia[] = {"rgr_guardia","Gesture"};
			rgr_guardia_loop[] = {"rgr_guardia_loop","Gesture"};
			rgr_guardia_halcon[] = {"rgr_guardia_halcon","Gesture"};
			rgr_guardia_halcon_loop[] = {"rgr_guardia_halcon_loop","Gesture"};
			rgr_GuardiaBaja[] = {"rgr_GuardiaBaja","Gesture"};
			rgr_paseoJedi[] = {"rgr_paseoJedi","Gesture"};
			fr4_fly_static[] = {"fr4_fly_static","Gesture"};
			fr4_fly_to_flyforward[] = {"fr4_fly_to_flyforward","Gesture"};
			fr4_flyforward[] = {"fr4_flyforward","Gesture"};
		};
	};
};
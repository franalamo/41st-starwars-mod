titletext ["","BLACK FADED", 0];
playmusic "intro";

_camera = "camera" camcreate [0,0,0];
_camera CameraEffect ["Internal","Back"];

_camera camPrepareTarget (getPosVisual unit);
_camera camPreparePos ([11069.9,7908.06,0.5]);
_camera camPrepareFOV 0.300;
_camera camCommitPrepared 0;
waitUntil {time > 0};
showCinemaBorder false;

sleep 3;

titletext ["","BLACK IN", 3];
"ace_goggles_GogglesLayer" cutFadeOut 0;
"ace_goggles_GogglesEffectsLayer" cutFadeOut 0;

_camera camPreparePos ([11065.4,7910.23,2]);
_camera camPrepareFOV 0.375;
_camera camCommitPrepared 42;

_camera camPrepareTarget (getPosVisual unit);
_camera camPrepareFOV 0.400;
_camera camCommitPrepared 30;
sleep 30;
titleCut ["","BLACK OUT", 1];
sleep 2;
_camera camPreparePos ([11145.6,8101.99,10]);
_camera camPrepareFOV 0.200;
_camera camCommitPrepared 0;

_camera camPrepareTarget (getPosVisual unit);
_camera camPrepareFOV 0.200;
_camera camCommitPrepared 0;

_camera camPreparePos ([11097.8,8075.57,5]);
_camera camPrepareFOV 0.200;
_camera camCommitPrepared 50;
sleep 0.1;
titleCut ["","BLACK IN", 0.3];
sleep 10;
0.9 fadesound 1;
sleep 0.1;
titleCut ["","BLACK OUT", 3];
sleep 3;
titletext ["<t size='20'><img image='41st_logo.paa' />", "BLACK FADED", 2, true, true];
exit;
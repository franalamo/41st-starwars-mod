class RscStandardDisplay;
class RscPicture;
class RscPictureKeepAspect;
class RscVignette;
class RscControlsGroupNoScrollbars;
class RscText;
class RscStructuredText;
class RscProgress;
class RscPictureload: RscPictureKeepAspect
{
	style="0x30 + 0x800";
	idc=999;
	text="#(argb,8,8,3)color(0,0,0,1)";
	colorText[]={1,1,1,1};
	x="safezoneX";
	y="safezoneY";
	w="0.5*(safezoneW)";
	h="0.5*(safezoneH)";

};
class RscControlsGroup;
class RscDisplayStart: RscStandardDisplay
{
	onLoad="[2] call compile preprocessfilelinenumbers gettext (configfile >> 'CfgFunctions' >> 'init');[1201,(_this) select 0,'RscDisplayStart'] call (uinamespace getvariable 'fr4_fnc_setRandomBackground');[1202,(_this) select 0,'RscDisplayStart'] call (uinamespace getvariable 'fr4_fnc_setRandomBackground')";
	onUnload="['onUnload',_this,'RscDisplayLoading'] call (uiNamespace getVariable 'full_mission_load_fnc_load')";

	class controlsBackground
	{
		class CA_Vignette: RscVignette
		{
			colorText[]={0,0,0,1};
		};
		class Map: RscPicture
		{
			idc=999;
			text="#(argb,8,8,3)color(0,0,0,1)";
			colorText[]={1,1,1,0.1};
			x="safezoneX";
			y="safezoneY - (safezoneW * 4/3) / 4";
			w="safezoneW";
			h="safezoneW * 4/3";
		};
		class Noise: RscPicture
		{
			idc=1202;
			text="";
			colorText[]={1,1,1,0.9};
			x="safezoneX";
			y="safezoneY";
			w="safezoneW";
			h="safezoneH";
		};
	};

	class controls
	{
		delete Text;
		delete Progress;
		delete Progress2;
		class LoadingStart: RscControlsGroup
		{
			idc = 2310;
			x = "0 * safezoneW + safezoneX";
			y = "0 * safezoneH + safezoneY";
			w = "1 * safezoneW";
			h = "1 * safezoneH";
			class controls
			{
				class Black: RscText
				{
					idc = 1000;
					x = "0 * safezoneW";
					y = "0 * safezoneH";
					w = "1 * safezoneW";
					h = "1 * safezoneH";
					colorBackground[] = {0,0,0,1};
				};
				class Noise: RscPicture
				{
					idc = 1201;
					text = "";
					x = "0 * safezoneW";
					y = "0 * safezoneH";
					w = "1 * safezoneW";
					h = "1 * safezoneH";
				};
				class Logo: RscPictureKeepAspect
				{
					idc = 1200;
					text = "\fr4_starwars_animations\data\41st_logo.paa";
					x = "0.25 * safezoneW";
					y = "0.3125 * safezoneH";
					w = "0.5 * safezoneW";
					h = "0.25 * safezoneH";
					onLoad = "if (395180 in getDLCs 1) then {(_this select 0) ctrlsettext '\fr4_starwars_animations\data\41st_logo.paa';};";
				};
			};
		};
	};
};
class RscDisplayLoadMission: RscStandardDisplay
{
	onLoad="['onload',_this,'RscDisplayLoading'] call (uiNamespace getVariable 'full_mission_load_fnc_load');[654,(_this) select 0,'RscDisplayLoadMission'] call (uinamespace getvariable 'fr4_fnc_setRandomBackground');[999,(_this) select 0,'RscDisplayLoadMission'] call (uinamespace getvariable 'fr4_fnc_setRandomBackground');[1201,(_this) select 0,'RscDisplayLoadMission'] call (uinamespace getvariable 'fr4_fnc_setRandomBackground');call (uiNamespace getVariable 'fr4_fnc_stopMusic');";
	onUnload="[""onUnload"",_this,""RscDisplayLoading""] call (uiNamespace getVariable 'full_mission_load_fnc_load')";
	class controlsBackground
	{
		class CA_Vignette: RscVignette
		{
			colorText[]={0,0,0,1};
		};
		class Map: RscPicture
		{
			idc=999;
			text="#(argb,8,8,3)color(0,0,0,1)";
			colorText[]={1,1,1,0};
			x="safezoneX";
			y="safezoneY - (safezoneW * 4/3) / 4";
			w="safezoneW";
			h="safezoneW * 4/3";
		};
		class Noise: RscPicture
		{
			idc=654;
			text="";
			colorText[]={1,1,1,1};
			x="safezoneX";
			y="safezoneY";
			w="safezoneW";
			h="safezoneH";
		};
	};
	class controls
	{
		delete Title;
		delete Name;
		delete Briefing;
		delete Progress;
		delete Progress2;
		delete Date;
		class MapBackTop: RscText
		{
			x = "safezoneX";
			y = "safezoneY";
			w = "safezoneW";
			idc = 1000;
			h = "2.7 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorBackground[] = {0,0,0,0};
		};
		class MapName: RscText
		{
			x = "safezoneX + 0.2 * 					(			((safezoneW / safezoneH) min 1.2) / 40)";
			y = "safezoneY";
			w = "safezoneW - 0.4 * 					(			((safezoneW / safezoneH) min 1.2) / 40)";
			idc = 1001;
			h = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			sizeEx = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ProgressNameMap: RscStructuredText
		{
			style = 2;
			x = "safezoneX";
			y = "safezoneY";
			w = "safezoneW";
			class Attributes
			{
				size = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				align = "right";
			};
			idc = 2421;
			h = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			sizeEx = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ProgressNameMission: RscStructuredText
		{
			style = 2;
			x = "safezoneX";
			y = "safezoneY";
			w = "safezoneW";
			class Attributes
			{
				size = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				align = "center";
			};
			idc = 2354;
			h = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			sizeEx = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class MapAuthor: RscText
		{
			x = "safezoneX + 0.2 * 					(			((safezoneW / safezoneH) min 1.2) / 40)";
			y = "safezoneY + 1.3 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			font = "RobotoCondensedLight";
			idc = 1002;
			w = "16 * 					(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "1 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorText[] = {1,1,1,0.5};
			sizeEx = "0.8 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class MapBackBottom: RscText
		{
			x = "safezoneX";
			y = "safezoneY + safezoneH - 2 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			w = "safezoneW";
			idc = 1003;
			h = "2 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorBackground[] = {0,0,0,0.6};
		};
		class MapDescription: RscStructuredText
		{
			style = 2;
			x = 0;
			y = "safezoneY + safezoneH - 1.75 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			w = 1;
			class Attributes
			{
				size = "0.8 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				align = "center";
			};
			idc = 1102;
			h = "1.75 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			sizeEx = "0.8 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ProgressMap: RscProgress
		{
			colorBar[] = {};
			texture = "\fr4_starwars_animations\data\loadingSword.paa";
			x = "safezoneX + 0.6";
			y = "safezoneY - 0.1";
			w = "safezoneW - 0.6";
			idc = 104;
			h = "6.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ProgressMission: ProgressMap
		{
			idc = 1013;
			x = "1.5 * 					(			((safezoneW / safezoneH) min 1.2) / 40) + 		(safezoneX + (safezoneW - 					((safezoneW / safezoneH) min 1.2))/2)";
			y = "10 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 		(safezoneY + (safezoneH - 					(			((safezoneW / safezoneH) min 1.2) / 1.2))/2)";
			w = "16 * 					(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "0.2 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class LoadingStart: RscControlsGroup
		{
			idc = 2310;
			x = "0 * safezoneW + safezoneX";
			y = "0 * safezoneH + safezoneY";
			w = "1 * safezoneW";
			h = "1 * safezoneH";
			class controls
			{
				class Black: RscText
				{
					idc = 1000;
					x = "0 * safezoneW";
					y = "0 * safezoneH";
					w = "1 * safezoneW";
					h = "1 * safezoneH";
					colorBackground[] = {0,0,0,1};
				};
				class Noise: RscPicture
				{
					idc = 1201;
					text = "";
					x = "0 * safezoneW";
					y = "0 * safezoneH";
					w = "1 * safezoneW";
					h = "1 * safezoneH";
				};
				class Logo: RscPictureKeepAspect
				{
					idc = 1200;
					text = "\fr4_starwars_animations\data\41st_logo_big.paa";
					x = "0.25 * safezoneW";
					y = "0.3125 * safezoneH";
					w = "0.5 * safezoneW";
					h = "0.25 * safezoneH";
					onLoad = "if (395180 in getDLCs 1) then {(_this select 0) ctrlsettext '\a3\Ui_f\Data\Logos\arma3apex_white_ca.paa';};";
				};
			};
		};
	};
};
class RscDisplayLoading
{
	onLoad="['onload',_this,'RscDisplayLoading'] call (uiNamespace getVariable 'full_mission_load_fnc_load');[102,(_this) select 0,'RscDisplayLoading'] call (uinamespace getvariable 'fr4_fnc_setRandomBackground')";
	onUnload="[""onUnload"",_this,""RscDisplayLoading""] call (uiNamespace getVariable 'full_mission_load_fnc_load')";

	class controlsBackground
	{
		class CA_Vignette: RscVignette
		{
			colorText[]={0,0,0,1};
		};
		class Map: RscPicture
		{
			idc=999;
			text="#(argb,8,8,3)color(0,0,0,1)";
			colorText[]={1,1,1,0.1};
			x="safezoneX";
			y="safezoneY - (safezoneW * 4/3) / 4";
			w="safezoneW";
			h="safezoneW * 4/3";
		};
		class Noise: RscPicture
		{
			idc=102;
			text="";
			colorText[]={1,1,1,0.9};
			x="safezoneX";
			y="safezoneY";
			w="safezoneW";
			h="safezoneH";
		};
	};
};
class RscDisplayNotFreeze: RscStandardDisplay
{
	onLoad="['onload',_this,'RscDisplayLoading'] call (uiNamespace getVariable 'full_mission_load_fnc_load');[102,(_this) select 0,'RscDisplayNotFreeze'] call (uinamespace getvariable 'fr4_fnc_setRandomBackground');[1201,(_this) select 0,'RscDisplayNotFreeze'] call (uinamespace getvariable 'fr4_fnc_setRandomBackground')";
	onUnload="[""onUnload"",_this,""RscDisplayLoading""] call (uiNamespace getVariable 'full_mission_load_fnc_load')";

	class controlsBackground
	{
		class CA_Vignette: RscVignette
		{
			colorText[]={0,0,0,1};
		};
		class Map: RscPicture
		{
			idc=999;
			text="#(argb,8,8,3)color(1,0,0,1)";
			colorText[]={1,1,1,0.1};
			x="safezoneX";
			y="safezoneY - (safezoneW * 4/3) / 4";
			w="safezoneW";
			h="safezoneW * 4/3";
		};
		class Noise: RscPicture
		{
			idc=102;
			text="";
			colorText[]={1,1,1,0.9};
			x="safezoneX";
			y="safezoneY";
			w="safezoneW";
			h="safezoneH";
		};
	};
	class controls
	{
		delete Progress2;
		delete Name;
		delete Date;
		delete Title;
		delete Progress;
		delete Briefing;
		delete Text;
		class MapBackTop: RscText
		{
			x = "safezoneX";
			y = "safezoneY";
			w = "safezoneW";
			idc = 1000;
			h = "2.7 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorBackground[] = {0,0,0,0};
		};
		class MapName: RscText
		{
			x = "safezoneX + 0.2 * 					(			((safezoneW / safezoneH) min 1.2) / 40)";
			y = "safezoneY";
			w = "safezoneW - 0.4 * 					(			((safezoneW / safezoneH) min 1.2) / 40)";
			idc = 1001;
			h = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			sizeEx = "1.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class MapBackBottom: RscText
		{
			x = "safezoneX";
			y = "safezoneY + safezoneH - 2 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			w = "safezoneW";
			idc = 1003;
			h = "2 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			colorBackground[] = {0,0,0,0.6};
		};
		class MapDescription: RscStructuredText
		{
			style = 2;
			x = 0;
			y = "safezoneY + safezoneH - 1.75 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			w = 1;
			class Attributes
			{
				size = "0.8 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
				align = "center";
			};
			idc = 1102;
			h = "1.75 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
			sizeEx = "0.8 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ProgressMap: RscProgress
		{
			colorBar[] = {};
			texture = "\fr4_starwars_animations\data\loadingSword.paa";
			x = "safezoneX + 0.6";
			y = "safezoneY - 0.1";
			w = "safezoneW - 0.6";
			idc = 104;
			h = "6.5 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class ProgressMission: ProgressMap
		{
			idc = 1013;
			x = "1.5 * 					(			((safezoneW / safezoneH) min 1.2) / 40) + 		(safezoneX + (safezoneW - 					((safezoneW / safezoneH) min 1.2))/2)";
			y = "10 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25) + 		(safezoneY + (safezoneH - 					(			((safezoneW / safezoneH) min 1.2) / 1.2))/2)";
			w = "16 * 					(			((safezoneW / safezoneH) min 1.2) / 40)";
			h = "0.2 * 					(			(			((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
		};
		class LoadingStart: RscControlsGroup
		{
			idc = 2310;
			x = "0 * safezoneW + safezoneX";
			y = "0 * safezoneH + safezoneY";
			w = "1 * safezoneW";
			h = "1 * safezoneH";
			class controls
			{
				class Black: RscText
				{
					idc = 1000;
					x = "0 * safezoneW";
					y = "0 * safezoneH";
					w = "1 * safezoneW";
					h = "1 * safezoneH";
					colorBackground[] = {0,0,0,1};
				};
				class Noise: RscPicture
				{
					idc = 1201;
					text = "";
					x = "0 * safezoneW";
					y = "0 * safezoneH";
					w = "1 * safezoneW";
					h = "1 * safezoneH";
				};
				class Logo: RscPictureKeepAspect
				{
					idc = 1200;
					text = "\fr4_starwars_animations\data\41st_logo.paa";
					x = "0.25 * safezoneW";
					y = "0.3125 * safezoneH";
					w = "0.5 * safezoneW";
					h = "0.25 * safezoneH";
					onLoad = "if (395180 in getDLCs 1) then {(_this select 0) ctrlsettext '\a3\Ui_f\Data\Logos\arma3apex_white_ca.paa';};";
				};
			};
		};
	};
};
	class RscDisplayLoadCustom: RscDisplayLoadMission
	{
		onLoad="['onload',_this,'RscDisplayLoading'] call (uiNamespace getVariable 'full_mission_load_fnc_load');[102,(_this) select 0,'RscDisplayLoadCustom'] call (uinamespace getvariable 'fr4_fnc_setRandomBackground')";
		onUnload="[""onUnload"",_this,""RscDisplayLoading""] call (uiNamespace getVariable 'full_mission_load_fnc_load')";

		class controlsBackground
		{
			class CA_Vignette: RscVignette
			{
				colorText[]={0,0,0,1};
			};
			class Map: RscPicture
			{
				idc=999;
				text="#(argb,8,8,3)color(0,0,0,1)";
				colorText[]={1,1,1,0.1};
				x="safezoneX";
				y="safezoneY - (safezoneW * 4/3) / 4";
				w="safezoneW";
				h="safezoneW * 4/3";
			};
			class Noise: RscPicture
			{
				idc=102;
				text="";
				colorText[]={1,1,1,0.9};
				x="safezoneX";
				y="safezoneY";
				w="safezoneW";
				h="safezoneH";
			};
		};
	};
	class RscActivePicture;
	class RscDisplayMain: RscStandardDisplay
	{
		onLoad = "[""onLoad"",_this,""RscDisplayMain"",'GUI'] call  (uinamespace getvariable 'BIS_fnc_initDisplay');[102,(_this) select 0,'RscDisplayMain'] call (uinamespace getvariable 'fr4_fnc_setRandomBackground')";
		class controlsBackground
		{
			class CA_Vignette: RscVignette
			{
				colorText[]={0,0,0,1};
			};
			class Map: RscPicture
			{
				idc=999;
				text="#(argb,8,8,3)color(0,0,0,1)";
				colorText[]={1,1,1,0};
				x="safezoneX";
				y="safezoneY - (safezoneW * 4/3) / 4";
				w="safezoneW";
				h="safezoneW * 4/3";
			};
			class Picture : RscPicture
			{
				idc=102;
				text="";
				colorText[]={1,1,1,1};
				x="safezoneX";
				y="safezoneY";
				w="safezoneW";
				h="safezoneH";
			};
		};
		class controls {
			class CAALogo : RscPictureKeepAspect
			{
				soundClick[] = {};
				soundEnter[] = {};
				text = "\fr4_starwars_animations\data\41st_logo.paa";
				tooltip = "41st - Cuerpo de Elite - Visita http://41st.es";
			};
			class Logo : RscActivePicture
			{
				action = "'http://41st.es/' call (uiNamespace getVariable 'fr4_fnc_openWebPage')";
				soundClick[] = {};
				soundEnter[] = {};
				text = "\fr4_starwars_animations\data\41st_logo_big.paa";
				tooltip = "41st - Cuerpo de Elite - Visita http://41st.es";
			};
		};
	};

	class RscBackgroundLogo : RscPictureKeepAspect
	{
		text = "\fr4_starwars_animations\data\41st_logo.paa";
	};

	class RscTitles
	{
		class fr4_jetpack_gui {
			idd = -1;
			duration = 1e+010;
			onLoad = "uiNamespace setVariable ['fr4_jetpack_gui',_this];((_this select 0) displayCtrl -1) progressSetPosition 1";
			class Controls
			{
				class Progress: RscProgress
				{
					colorBar[] = {};
					texture = "\fr4_starwars_animations\data\progress_bar.paa";
					x = "safezoneX";
					y = "safezoneY + safezoneH - 0.4";
					w = "0.24";
					idc = -1;
					h = "0.08";
				};
			};
		};

		class RscDisplayMainMenuBackground
		{
			scriptName = "RscDisplayMainMenuBackground";
			scriptPath = "GUI";
			onLoad = "[""onLoad"",_this,""RscDisplayMainMenuBackground"",'GUI'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay');[102,(_this) select 0,'RscDisplayMainMenuBackground'] call (uinamespace getvariable 'fr4_fnc_setRandomBackground')";
			onUnload = "[""onUnload"",_this,""RscDisplayMainMenuBackground"",'GUI'] call 	(uinamespace getvariable 'BIS_fnc_initDisplay')";
			idd = -1;
			fadein = 2;
			fadeout = 2;
			duration = 1e+010;
			class Controls
			{
				class Background: RscText
				{
					idc = 101;
					colorBackground[] = {0.1,0.1,0.1,1};
					x = "safezoneXAbs";
					y = "-	10";
					w = "safezoneWAbs";
					h = "2 * 	10";
				};
				class BackgroundLeft: Background
				{
					x = "-	10";
					w = "safezoneX + 	10";
				};
				class BackgroundRight: Background
				{
					x = "safezoneX + safezoneW";
					w = 10;
				};
				class Picture: RscPicture
				{
					idc = 102;
					text = "";
					x = "safezoneX";
					y = "safezoneY";
					w = "safezoneW";
					h = "safezoneH";
				};
			};
		};
		class SplashNoise
		{
			onLoad="[2] call compile preprocessfilelinenumbers gettext (configfile >> 'CfgFunctions' >> 'init'); 'introMusic.wav' call (uiNamespace getVariable 'fr4_fnc_startMusic');[1,(_this) select 0,'SplashNoise'] call (uinamespace getvariable 'fr4_fnc_setRandomBackground')";
			idd = -1;
			movingEnable = 0;
			duration = 11;
			fadein = 2;
			fadeout = 2;
			controls[] = {"BackgroundNoise"};
			class BackgroundNoise: RscPicture
			{
				idc = 1
				text = "";
				colorText[] = {1,1,1,0.7};
				x = "safezoneXAbs";
				y = "safezoneY";
				w = "safezoneWAbs";
				h = "safezoneH";
			};
		};
		class SplashBohemia
		{
			idd = -1;
			movingEnable = 0;
			duration = 0;
			fadein = 0;
			fadeout = 0;
			name = "";
			controls[] = {"Picture","TextPresents"};
			class Picture: RscPicture
			{
				idc = 1200;
				text = "\fr4_starwars_animations\data\41st_logo.paa";
				x = "SafeZoneX + SafeZoneW / 2 - (48 * 		(0.01875 * SafezoneH)) / 2";
				y = "SafeZoneY +	SafeZoneH / 2 - (31.7 * 		(0.025 * SafezoneH)) / 2";
				w = "48 * 		(0.01875 * SafezoneH)";
				h = "24 * 		(0.025 * SafezoneH)";
			};
			class TextPresents: RscText
			{
				idc = 1000;
				text = "";
				style = 2;
				sizeEx = "0.03*SafezoneH";
				shadow = 0;
				x = "SafeZoneX + SafeZoneW / 2 - (46.5 * 		(0.01875 * SafezoneH)) / 2";
				y = "SafeZoneY +	SafeZoneH - (0.505 * SafezoneH)";
				w = "48 * 		(0.01875 * SafezoneH)";
				h = "0.03 * SafezoneH";
			};
		};
		class SplashArma3
		{
			idd = -1;
			movingEnable = 0;
			duration = 3;
			fadein = 2;
			fadeout = 2;
			name = "";
			controls[] = {"Picture"};
			class Picture: RscPicture
			{
				idc = 1200;
				text = "\fr4_starwars_animations\data\41st_logo.paa";
				x = "0.5 - 10 * 		(0.01875 * SafezoneH)";
				y = "0.5 - 6 * 		(0.025 * SafezoneH)";
				w = "20 * 		(0.01875 * SafezoneH)";
				h = "10 * 		(0.025 * SafezoneH)";
			};
		};
		class SplashArma3Apex: SplashArma3
		{
			class Picture: Picture
			{
				text = "\fr4_starwars_animations\data\41st_logo.paa";
				y = "0.5 - 7 * 		(0.025 * SafezoneH)";
			};
		};
		class SplashArma3Lite
		{
			idd = -1;
			movingEnable = 0;
			duration = 3;
			fadein = 2;
			fadeout = 2;
			controls[] = {"Picture","Tagline"};
			class Picture: RscPicture
			{
				idc = 1200;
				text = "\fr4_starwars_animations\data\41st_logo_big.paa";
				x = "SafeZoneX + SafeZoneW / 2 - (20 * 		(0.01875 * SafezoneH)) / 2";
				y = "SafeZoneY +	SafeZoneH / 2 - (17 * 		(0.025 * SafezoneH)) / 2";
				w = "20 * 		(0.01875 * SafezoneH)";
				h = "10 * 		(0.025 * SafezoneH)";
			};
			class Tagline: RscPicture
			{
				idc = 1201;
				text = "\fr4_starwars_animations\data\41st_logo_big.paa";
				x = "SafeZoneX + SafeZoneW / 2 - (16.1 * 		(0.01875 * SafezoneH)) / 2";
				y = "SafeZoneY +	SafeZoneH / 2 - (2.9 * 		(0.025 * SafezoneH)) / 2";
				w = "16.1 * 		(0.01875 * SafezoneH)";
				h = "8.05 * 		(0.025 * SafezoneH)";
			};
		};
		class SplashTagline
		{
			idd = -1;
			movingEnable = 0;
			duration = 3;
			fadein = 2;
			fadeout = 2;
			name = "$STR_A3_SplashTagline_0";
			controls[] = {"Picture"};
			class Picture: RscPicture
			{
				idc = 1200;
				text = "\fr4_starwars_animations\data\41st_logo_big.paa";
				x = "SafeZoneX + SafeZoneW / 2 - (48 * 		(0.01875 * SafezoneH)) / 2";
				y = "SafeZoneY +	SafeZoneH / 2 - (31.7 * 		(0.025 * SafezoneH)) / 2";
				w = "48 * 		(0.01875 * SafezoneH)";
				h = "24 * 		(0.025 * SafezoneH)";
			};
		};
		class SplashESRB
		{
			idd = -1;
			movingEnable = 0;
			duration = 5;
			fadein = 2;
			fadeout = 2;
			name = "$STR_A3_SplashESRB_0";
			controls[] = {"Picture"};
			class Picture: RscPicture
			{
				text = "\fr4_starwars_animations\data\41st_logo_big.paa";
				x = "SafeZoneX + SafeZoneW / 2 - (72 * 		(0.01875 * SafezoneH)) / 2";
				y = "SafeZoneY +	SafeZoneH / 2 - (36 * 		(0.025 * SafezoneH)) / 2";
				w = "72 * 		(0.01875 * SafezoneH)";
				h = "36 * 		(0.025 * SafezoneH)";
			};
		};
		class SplashCopyright
		{
			idd = -1;
			movingEnable = 0;
			duration = 5;
			fadein = 2;
			fadeout = 2;
			name = "$STR_A3_SplashCopyright_0";
			controls[] = {"PictureA3Logo","PictureBIlogo","PicturePhysXLogo","LegalText"};
			class PictureA3Logo: RscPicture
			{
				idc = 1200;
				text = "";
				x = "SafeZoneX + SafeZoneW / 2 - (36 * 		(0.01875 * SafezoneH)) / 2";
				y = "SafeZoneY +	SafeZoneH / 2 - (13.7 * 		(0.025 * SafezoneH)) / 2";
				w = "8 * 		(0.01875 * SafezoneH)";
				h = "4 * 		(0.025 * SafezoneH)";
			};
			class PictureBIlogo: RscPicture
			{
				idc = 1201;
				text = "\fr4_starwars_animations\data\41st_logo_big.paa";
				x = "SafeZoneX + SafeZoneW / 2 - (33 * 		(0.01875 * SafezoneH)) / 2";
				y = "SafeZoneY +	SafeZoneH / 2 - (25.7 * 		(0.025 * SafezoneH)) / 2";
				w = "32 * 		(0.01875 * SafezoneH)";
				h = "16 * 		(0.025 * SafezoneH)";
			};
			class PicturePhysXLogo: RscPicture
			{
				idc = 1202;
				text = "";
				x = "SafeZoneX + SafeZoneW / 2 + (16.5 * 		(0.01875 * SafezoneH)) / 2";
				y = "SafeZoneY +	SafeZoneH / 2 - (13.6 * 		(0.025 * SafezoneH)) / 2";
				w = "16 * 		(0.01875 * SafezoneH)";
				h = "4 * 		(0.025 * SafezoneH)";
			};
			class LegalText: RscStructuredText
			{
				idc = -1;
				text = "41st - Cuerpo de Elite";
				size = "0.02 * SafezoneH";
				shadow = 0;
				x = "SafeZoneX + SafeZoneW / 2 - (50 * 		(0.01875 * SafezoneH)) / 2";
				y = "SafeZoneY +	SafeZoneH - (0.38 * SafezoneH)";
				w = "50 * 		(0.01875 * SafezoneH)";
				h = "0.39 * SafezoneH";
				class Attributes
				{
					align = "center";
					shadow = 0;
				};
			};
		};
	};
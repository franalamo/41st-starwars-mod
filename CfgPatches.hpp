class CfgPatches
{
	class fr4_starwars_animations
	{
		requiredaddons[] = {"CAAModerna_client_SWOP","SWOP_Main","A3_Weapons_F","CBA_MAIN","ace_main","SWOP_Characters_JEDI"};
		requiredversion = 1.0;
		units[] = {""};
		weapons[] = {""};
		magazines[] = {""};
		url = "http://41st.es";
		version = "0.0.1";
		versionAr[] = {0,0,1};
		versionStr = "0.0.1";
	};
};
class CfgVehicles
{
	class Man;
	class CAManBase: Man
	{
		class ACE_SelfActions
		{
			class rgr_anim
			{
				displayName = "41st Animations";
				icon = "\fr4_starwars_animations\data\41st_logo_small.paa";
				class rgr_jedi
				{
					displayName = "Jedi";
					class rgr_sword_on
					{
						displayName = "On";
						condition = "((handgunWeapon _player) find 'lightsaber') >=0 && ((count (handgunMagazine _player)) == 0)";
						exceptions[] = {};
						statement = "_player addHandgunItem ((getArray (configFile >> 'CfgWeapons' >> (handgunWeapon _player) >> 'magazines')) select 0)";
					};
					class rgr_sword_off
					{
						displayName = "Off";
						condition = "((handgunWeapon _player) find 'lightsaber') >=0 && ((count (handgunMagazine _player)) > 0)";
						exceptions[] = {};
						statement = "_player removeHandgunItem (currentMagazine _player)";
					};
					class rgr_guardia_pose
					{
						displayName = "Guardia";
						condition = "";
						exceptions[] = {};
						statement = "[_player] spawn {(_this select 0) playActionNow 'rgr_guardia';sleep 1;(_this select 0) playActionNow 'rgr_guardia_loop';};";
					};
					class rgr_guardia_halcon_pose
					{
						displayName = "Guardia Halcon";
						condition = "";
						exceptions[] = {};
						statement = "[_player] spawn {(_this select 0) playActionNow 'rgr_guardia_halcon';sleep 1;(_this select 0) playActionNow 'rgr_guardia_halcon_loop';};";
					};
					class rgr_guardia_baja
					{
						displayName = "Guardia Baja";
						condition = "";
						exceptions[] = {};
						statement = "[_player] spawn {(_this select 0) playActionNow 'rgr_GuardiaBaja'};";
					};
					class rgr_encendido_chulo
					{
						displayName = "Encendido";
						condition = "";
						exceptions[] = {};
						statement = "[_player] spawn {[ace_player, 'rgr_encendido'] remoteExec ['switchMove', 0];};";
					};
					class rgr_obiwan
					{
						displayName = "Obi-Wan";
						condition = "";
						exceptions[] = {};
						statement = "[_player] spawn {[ace_player, 'rgr_obiwan_point'] remoteExec ['switchMove', 0];sleep 1;[ace_player, 'rgr_obiwan_point_loop'] remoteExec ['switchMove', 0];};";
					};
				};
				class rgr_desarmado
				{
					displayName = "Desarmado";
					class rgr_pensativo
					{
						displayName = "Pensativo";
						condition = "";
						exceptions[] = {};
						statement = "[ace_player, 'rgr_pensativo'] remoteExec ['switchMove', 0]";
					};
					class rgr_estrangulacion
					{
						displayName = "Estrangulacion";
						condition = "";
						exceptions[] = {};
						statement = "[_player] spawn {[ace_player, 'rgr_estrangulacion'] remoteExec ['switchMove', 0];};";
					};
					class rgr_choquepunch //TODO
					{
						displayName = "ChoquePunch";
						condition = "";
						exceptions[] = {};
						statement = "hint 'No implementada todavia'";
					};
					class rgr_silencio //TODO
					{
						displayName = "Silencio";
						condition = "";
						exceptions[] = {};
						statement = "hint 'No implementada todavia'";
					};
					class rgr_paseo_jedi
					{
						displayName = "Paseo Jedi";
						condition = "";
						exceptions[] = {};
						statement = "[_player] spawn {(_this select 0) playActionNow 'rgr_paseoJedi'};";
					};
					class rgr_brazoscruzados
					{
						displayName = "Brazos Cruzados";
						condition = "";
						exceptions[] = {};
						statement = "[ace_player, 'rgr_brazoscruzados'] remoteExec ['switchMove', 0]";
					};
					class rgr_cinturon
					{
						displayName = "Cinturon";
						condition = "";
						exceptions[] = {};
						statement = "[ace_player, 'rgr_cinturon'] remoteExec ['switchMove', 0]";
					};
					class rgr_espera
					{
						displayName = "Espera";
						condition = "";
						exceptions[] = {};
						statement = "[ace_player, 'rgr_espera'] remoteExec ['switchMove', 0]";
					};
					class rgr_facepalm
					{
						displayName = "Facepalm";
						condition = "";
						exceptions[] = {};
						statement = "[ace_player, 'rgr_facepalm'] remoteExec ['switchMove', 0]";
					};
					class rgr_victoria
					{
						displayName = "Victoria";
						condition = "";
						exceptions[] = {};
						statement = "[ace_player, 'rgr_victoria'] remoteExec ['switchMove', 0]";
					};
					class rgr_meditar
					{
						displayName = "Meditar";
						condition = "";
						exceptions[] = {};
						statement = "[_player] spawn {[ace_player, 'rgr_meditar'] remoteExec ['switchMove', 0];sleep 1;[ace_player, 'rgr_meditar_loop'] remoteExec ['switchMove', 0];};";
					};
					class rgr_firme
					{
						displayName = "Firme";
						condition = "";
						exceptions[] = {};
						statement = "hint 'No implementada todavia'";
					};
				};
				class rgr_fusil
				{
					displayName = "Fusil";
					class rgr_paseo_fusil //TODO
					{
						displayName = "Paseo Fusil";
						condition = "";
						exceptions[] = {};
						statement = "hint 'No implementada todavia'";
					};
					class rgr_fusil_hombro //TODO
					{
						displayName = "Fusil sobre el hombro";
						condition = "";
						exceptions[] = {};
						statement = "hint 'No implementada todavia'";
					};
					class rgr_fusil_suelo //TODO
					{
						displayName = "Fusil apoyado suelo";
						condition = "";
						exceptions[] = {};
						statement = "hint 'No implementada todavia'";
					};

				};
				class rgr_cancel
				{
					displayName = "Cancelar";
					icon = "\fr4_starwars_animations\data\ui_cancel.paa";
					condition = "";
					exceptions[] = {};
					statement = "_player playActionNow 'GestureNod';[ace_player, ''] remoteExec ['switchMove', 0]";
				};
			};
		};
	};

};
